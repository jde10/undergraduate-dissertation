# README #

This is the README for the repository for the undergraduate dissertation for graduating as a biomedical engineer from the University of the Andes.

### Contents ###

* Dissertation Document
* Version 1
* Version 2

### Dissertation Document ###

The document contains:

* Background information and motivation for the project.
* Database selection.
* Information on how to use Version 1, how to run each of the files.
* Results obtained using Version 1.
* Analysis of the results obtained and conclusions.

### Version 1 ###

This first version contains the following consecutive steps:

* Pre processing stage
* Candidate extraction stage
* Feature extraction stage
* SVM training

The highlight of this version was that with it we achieved a 96.75% recall over the LIDC database.

### Version 2 ###

This version contains a few improvements to the first version:

* Improving Precision
* Hard Negative Mining

The precision is improved from less than 1% to almost 90% by removing the pre processing of the CT scans after the candidate extraction. In version 1, a pre processing will filter the CT scans, and extract the lung tissue. the preprocessing will make that after the features that are extracted contain information that differs from reality and confuse the SVM. By extracting the lung tissue, the lungs will be surrounded by voxels that are all 0, and this was something that confused the SVM. Also, the filter possible removed details that would be important for recognising the nodules in the CT scans. 

Version 2 keeps the pre processing stage for candidate extraction, however for the feature extraction it uses isometrical CT scans that are just the same as the original CT scans, only that all of them are now the same size. The isometrical CT scans are not filtered, nor are the lungs extracted. This improved greatly the precision. Also, in this version, only the Histograms of Oriented Gradients (HOG) are used for feature extraction. It was noted that the combination of Pyramidal Histograms of Words (PHOW) and HOG gave a result that was inferior to that of HOG alone, and that the result of PHOW was lower than that of HOG. 

Hard negative mining was also used in order to train the SVM. The hard negative algorithm can be initiated using the file initiate.m. 

Finally, the results over the validation set composed of 252 CT scans are available.

### Future Work ###

In the near future, the results over the test set, made up of 504 CT scans, will be available. Also, a new solution using deep learning will be implemented. This solution will not only detect nodules, it will offer the specialist more information regarding whether the nodule is benign or malignant. An alliance with two local clinics has been made, which will further augment the database.

### Author ###

Juliana De La Vega
email: j.de10@uniandes.edu.co