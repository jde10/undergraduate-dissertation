%Load data
folder = ('/datos1/DATA/LIDC-IDRI_STD02/test/');
filter_folder = ('/datos1/DATA/matlab/DATAV2/preprocess_filter/test/');
mask_folder = ('/datos1/DATA/matlab/DATAV2/preprocess_mask/test/');
candidate_folder = ('/datos1/DATA/matlab/DATAV2/candidate_extraction/test/');
directory = dir('/datos1/DATA/LIDC-IDRI_STD02/test/*.mat');
sDir = length(directory);
r = 10;

parfor k=1:sDir,
    imDir = strcat(folder, directory(k).name);
    S = load(imDir);
    extract_candidates(S, mask_folder, directory(k).name, candidate_folder, r)
end
    
