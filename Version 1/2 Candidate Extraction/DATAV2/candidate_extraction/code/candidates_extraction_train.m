%Load data
folder = ('/datos1/DATA/LIDC-IDRI_STD02/train/');
filter_folder = ('/datos1/DATA/matlab/DATAV2/preprocess_filter/train/');
mask_folder = ('/datos1/DATA/matlab/DATAV2/preprocess_mask/train/');
candidate_folder = ('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/');
directory = dir('/datos1/DATA/LIDC-IDRI_STD02/train/*.mat');
sDir = length(directory);
r = 10;

parfor k=1:sDir,
    imDir = strcat(folder, directory(k).name);
    S = load(imDir);
    extract_candidates(S, mask_folder, directory(k).name, candidate_folder, r)
end
    
