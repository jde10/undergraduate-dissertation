function extract_candidates(S,mask_folder, name, candidate_folder, r)

vS = size(S.vol);
nS = size(S.nodules)
existNodule = 1;
%reorganize nodules
nodule = {};
%%use i and j for included annotations
i=1;
for j=1:nS(1)
    if S.nodules(j).included == 1
        nodule{i,1} = S.nodules(j).Centroid(2);
        nodule{i,2} = S.nodules(j).Centroid(1);
        nodule{i,3} = S.nodules(j).Centroid(3);
        i=i+1;
    end
end
nodule = cell2mat(nodule);
nodS = size(nodule);


load(strcat(mask_folder, name))
z = (S.z_pos(1) - S.z_pos(2));%para z dinamico
%z=1; %para bola de r1
%lungData=volmask;
%Opening by reconstruction

%str = strel('ball', 2, 4, 0);
tic
str = ball_strel(1, z);

marker = imerode(lungData, str);

cand = imreconstruct(marker, lungData, 6);

bw_cand = imregionalmax(cand, 6);
disp('time elapsed in opening by breconstruction')
toc
% Connected Components
tic
CC = bwconncomp(bw_cand, 26);
centroids = regionprops(CC, 'Centroid');
L = labelmatrix(CC);

TP = 0;
FP = 0;
disp('time calculating connected components')
toc



minDist = zeros(nodS(1), 1);
tic
for i=1:nodS(1)
    minDist(i) = inf;
end

d = zeros(numel(centroids), nodS(1));
trueNod = zeros(1, 3);
missingNod = zeros(1, 3);

missing = 1;


for i=1:nodS(1)
    nod =nodule(i,:);
    for j=1:numel(centroids);
        
        dataPoints= vertcat(nod, centroids(j).Centroid);
        
        dist = pdist(dataPoints);
        d(j,i) = dist;
        if(dist <= minDist(i))% && dist <= r)
            minDist(i) = dist;
            %trueNod(i) = j;
        end
    end
    clear nod
end

TP = 0;
FN = 0;

for i=1:nodS(1),
    nod =nodule(i,:);
    bTP = TP;
    for j=1:numel(centroids)
        if(d(j,i) == minDist(i) && d(j,i) <= r)
            TP = TP + 1;
            trueNod(TP,:) = nod;
        end
    end
    if bTP == TP
        FN = FN + 1;
        missingNod(FN,:) = nod;
    end
    clear nod
end
disp('time elapsed searching for minimum distance')
toc
disp(name)
disp('Included Nodules')
disp(nodS(1))
disp('True positives')
disp(TP)
disp('Misses')
disp(FN)

FP = numel(centroids) - TP;


namedir = strcat(candidate_folder, name);
tic

if (FN ~= 0)
    minDist=single(minDist); trueNod = single(trueNod); missingNod = single(missingNod); d = single(d); z = single(z);
    save(namedir, 'centroids', 'CC', 'minDist', 'trueNod', 'nodule', 'missingNod', 'd', 'z');
else
    minDist=single(minDist); trueNod = single(trueNod); d = single(d); z = single(z);
    
    save(namedir, 'centroids', 'CC', 'minDist', 'trueNod', 'nodule', 'd', 'z');
end

disp('time elapsed saving')
toc

