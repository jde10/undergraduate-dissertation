clear all; close all; clc

%Load data
folder = ('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/');
directory = dir('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/*.mat');
sDir = length(directory);
r = 10;
TPcount = 1;
FPcount = 1;
nodCount = 0;

for k=1:sDir,
    imDir = strcat(folder, directory(k).name);
    S = load(imDir);
    sD = size(S.d);
    sN = size(S.nodule);
    for j=1:sD(2)
        unique = 0;
    for i=1:numel(S.centroids),
        if (S.minDist(j) <= r && S.d(i,j) == S.minDist(j))
            if(unique == 0)
                TParr(TPcount) = S.d(i,j);
                TPcount = TPcount + 1;
                unique =1;
            end
        else
            FParr(FPcount) = S.d(i,j);
            FPcount = FPcount + 1;
        end
    end 
    end
    nodCount = sN(1) + nodCount;
    clear sN S
end

x = vertcat(TParr', FParr');
TPones = ones(numel(TParr), 1);
FPzeros = zeros(numel(FParr),1);
y = vertcat(TPones, FPzeros);


[AP,P,R] = scores2AP_100(x,y,nodCount);
plot(R,P)
xlabel('Recall'); ylabel('Precision')
title('ROC for classification by logistic regression')

print('-dpng', 'PRcurve_extract_candidates_DATAV2.png')

maxRecall = max(R(:))

save('extract_results_Train_DATAV2.mat', 'TParr', 'FParr', 'nodCount', 'maxRecall', '-v7.3')

% clear all
% 
% %Load 2D data
% folder = ('C:\Users\PC12\Documents\Candidates\TM_1ball_medfilt\');
% directory = dir('C:\Users\PC12\Documents\Candidates\TM_1ball_medfilt\*.mat');
% sDir = length(directory);
% r = 10;
% TPcount = 1;
% FPcount = 1;
% nodCount = 0;
% 
% for k=1:sDir,
%     imDir = strcat(folder, directory(k).name);
%     S = load(imDir);
%     sD = size(S.d);
%     sN = size(S.nodule);
%     for j=1:sD(2)
%         unique = 0;
%     for i=1:numel(S.centroids),
%         if (S.minDist(j) <= r && S.d(i,j) == S.minDist(j))
%             if(unique == 0)
%                 TParr(TPcount) = S.d(i,j);
%                 TPcount = TPcount + 1;
%                 unique =1;
%             end
%         else
%             FParr(FPcount) = S.d(i,j);
%             FPcount = FPcount + 1;
%         end
%     end 
%     end
%     nodCount = sN(1) + nodCount;
%     clear sN S
% end
% sF = size(FParr);
% x = vertcat(TParr', FParr');
% TPones = ones(numel(TParr), 1);
% FPzeros = zeros(numel(FParr),1);
% y = vertcat(TPones, FPzeros);
% 
% 
% [AP,P,R] = scores2AP_100(x,y,nodCount);
% plot(R,P)
% xlabel('Recall'); ylabel('Precision')
% title('ROC for classification by logistic regression')
% 
% print('-dpng', 'PRcurve_includedNodules.png')
% 
% maxRecall = max(R(:))
% 
% save('TM_extract_results_ballr1_medfilt.mat', 'TParr', 'FParr', 'sF', 'nodCount', 'maxRecall', '-v7.3')
% 
% clear all
% 
% %Load 2D data
% folder = ('C:\Users\PC12\Documents\Candidates\TM_1ballz\');
% directory = dir('C:\Users\PC12\Documents\Candidates\TM_1ballz\*.mat');
% sDir = length(directory);
% r = 10;
% TPcount = 1;
% FPcount = 1;
% nodCount = 0;
% 
% for k=1:sDir,
%     imDir = strcat(folder, directory(k).name);
%     S = load(imDir);
%     sD = size(S.d);
%     sN = size(S.nodule);
%     for j=1:sD(2)
%         unique = 0;
%     for i=1:numel(S.centroids),
%         if (S.minDist(j) <= r && S.d(i,j) == S.minDist(j))
%             if(unique == 0)
%                 TParr(TPcount) = S.d(i,j);
%                 TPcount = TPcount + 1;
%                 unique =1;
%             end
%         else
%             FParr(FPcount) = S.d(i,j);
%             FPcount = FPcount + 1;
%         end
%     end 
%     end
%     nodCount = sN(1) + nodCount;
%     clear sN S
% end
% sF = size(FParr);
% x = vertcat(TParr', FParr');
% TPones = ones(numel(TParr), 1);
% FPzeros = zeros(numel(FParr),1);
% y = vertcat(TPones, FPzeros);
% 
% 
% [AP,P,R] = scores2AP_100(x,y,nodCount);
% plot(R,P)
% xlabel('Recall'); ylabel('Precision')
% title('ROC for classification by logistic regression')
% 
% print('-dpng', 'PRcurve_includedNodules.png')
% 
% maxRecall = max(R(:))
% 
% save('TM_extract_results_ballr1z.mat', 'TParr', 'FParr','sF', 'nodCount', 'maxRecall', '-v7.3')
% 
% clear all
