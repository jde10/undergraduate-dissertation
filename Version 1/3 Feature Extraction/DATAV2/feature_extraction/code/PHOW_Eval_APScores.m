
load('noduleValidationScores.mat')

%GT = [ones(1,length(hogScores)), zeros(1,length(hogScoresN))];
Thresh = min(scores):0.1:max(scores);
TP = zeros(length(Thresh), 1); 
FN = zeros(length(Thresh), 1) ;
FP = zeros(length(Thresh), 1);
Precision = zeros(length(Thresh), 1);
Recall = zeros(length(Thresh), 1);
GTot = length(GT);



for j=1:length(Thresh),
S(j,:) = scores>Thresh(j);
for i=1:GTot,
if(GT(i) == double(S(j,i))),
TP(j) = TP(j) +1;
elseif (double(S(j,i) ==1) && GT(i) == 0)
FN(j) = FN(j) +1;
elseif (double(S(j,i) == 0) && GT(i) == 1)
FP(j) = FP(j) + 1;
end
end
Precision(j) = TP(j)/(TP(j)+FP(j));
Recall(j) = TP(j)/GTot;
end
figure(1)
plot(Recall, Precision)
title('Precision Recall Validation Set HOG and PHOW')
print('-dpng', 'PR_feature_validation.png')