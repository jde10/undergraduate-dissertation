clear all
close all
clc

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';
baseD1 = '/datos1/DATA/matlab/DATAV2/preprocess_filter/train/';%Filtered_Lung_NoMask/'; %'C:/Users/PC12/Documents/LIDC-IDRI_STD01/Train/';
D0 = dir('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/*.mat');
dirSize = length(D0);
cdata=0;
cdatan=0;
pic = {};

% negs(1) = [1 1 1];

for l=1:dirSize,
    
    %--------------------------------------------------------------------------
    % Part I : Load files
    %--------------------------------------------------------------------------
    C = load(strcat(baseD0,D0(l).name));
    CT = load(strcat(baseD1, strtok(D0(l).name, '.'), '.mat'));
    
    CT.vol = CT.lungData;
    
    name = strtok(strtok(D0(l).name, '_'), '.');
    %--------------------------------------------------------------------------
    % Part II : Extract XYZ Images of Nodules
    %--------------------------------------------------------------------------
    r=10;
    disSize = size(C.d);
    nodSize = size(C.nodule);
    volSize = size(CT.vol)
    for l=1:disSize(2)
        unique = 0;
        
        C.nodule(l,1) = round(C.nodule(l,1));
        C.nodule(l,2) = round(C.nodule(l,2));
        C.nodule(l,3) = round(C.nodule(l,3));
        
        zcut = CT.vol(:,:,C.nodule(l,3));
        xcut = fliplr(rot90((squeeze(CT.vol(:,C.nodule(l,1),:))),3));
        ycut = fliplr(rot90((squeeze(CT.vol(C.nodule(l,2),:,:))),3));
        
        count = 0;
        pic = {};
        for k=-1:1,
            for j=-1:1,
                for i=-1:1,
                    
                    if (volSize(1)>(C.nodule(l,1)+i+16) && (C.nodule(l,1)+i-15)>0 && volSize(2)>(C.nodule(l,2)+j+16) && (C.nodule(l,2)+j-15)>0 && volSize(3)>(C.nodule(l,3)+k+16) && (C.nodule(l,3)+k-15)>0 )
                        
                        count = count + 1;
                        pic{count} = zcut(C.nodule(l,2)+j-15:C.nodule(l,2)+j+16, C.nodule(l,1)+i-15:C.nodule(l,1)+i+16);%xy image
                        count = count + 1;
                        pic{count} = squeeze(xcut(C.nodule(l,3)+k-15:C.nodule(l,3)+k+16, C.nodule(l,2)+j-15:C.nodule(l,2)+j+16));%yz image
                        count = count + 1;
                        pic{count} = squeeze(ycut(C.nodule(l,3)+k-15:C.nodule(l,3)+k+16, C.nodule(l,1)+i-15:C.nodule(l,1)+i+16));%xz image
                        
                        if k==1 && j==1 && i==1,
                        cdata=cdata+1;
                        pic = cat(3, pic{:});
                        Train(cdata).data = single(pic);
                        Train(cdata).label = 1;%nodule
                        Train(cdata).origin = name;
                        Train(cdata).nodule = C.nodule(l,:);
                        end
                    end
                end
            end
        end
        
        
        
        clear zcut xcut ycut zpic xpic ypic
        
        
        
    end
    
    clear C CT name disSize nodSize negs
    
    
end

save('TrainData_filter_32x32_jitter_Ann.mat', 'Train', '-v7.3')

