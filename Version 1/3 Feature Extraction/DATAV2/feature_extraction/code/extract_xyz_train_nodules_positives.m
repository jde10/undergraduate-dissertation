clear all
close all
clc

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';
baseD1 = '/datos1/DATA/matlab/DATAV2/preprocess_mask/train/'; %'C:/Users/PC12/Documents/LIDC-IDRI_STD01/Train/';
D0 = dir('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/*.mat');
dirSize = length(D0);
cdata=0;
cdatan=0;

% negs(1) = [1 1 1];

for k=1:2:dirSize,
    
    %--------------------------------------------------------------------------
    % Part I : Load files
    %--------------------------------------------------------------------------
    C = load(strcat(baseD0,D0(k).name));
    CT = load(strcat(baseD1, strtok(D0(k).name, '.'), '.mat'));
    CT.vol = CT.lungData;
    
    name = strtok(strtok(D0(k).name, '_'), '.');
    %--------------------------------------------------------------------------
    % Part II : Extract XYZ Images of Nodules
    %--------------------------------------------------------------------------
    r=10;
    disSize = size(C.d);
    nodSize = size(C.nodule);
    volSize = size(CT.vol)
    for j=1:disSize(2)
        unique = 0;
        
        for i=1:numel(C.centroids),
            feature(i).Label = 0;
            feature(i).Centroid = C.centroids(i);
            if (C.minDist(j) <= r && C.d(i,j) == C.minDist(j))
                if(unique == 0)
                    trueCentroid(j)=i;
                    feature(i).Label = 1; % 1 = True Nodule
                    unique =1;
                    C.centroids(i).Centroid(1) = round(C.centroids(i).Centroid(1));
                    C.centroids(i).Centroid(2) = round(C.centroids(i).Centroid(2));
                    C.centroids(i).Centroid(3) = round(C.centroids(i).Centroid(3));
                    
                    zcut = CT.vol(:,:,C.centroids(i).Centroid(3));
                    xcut = fliplr(rot90((squeeze(CT.vol(:,C.centroids(i).Centroid(1),:))),3));
                    ycut = fliplr(rot90((squeeze(CT.vol(C.centroids(i).Centroid(2),:,:))),3));
                    
                    count = 0;
                    pic = {};
                    
                    for kk=1:1,
                        for jj=1:1,
                            for ii=1:1,
                                
                                
                                if (volSize(1)>C.centroids(i).Centroid(1)+ii+16 && (C.centroids(i).Centroid(1)+ii-15)>0 && volSize(2)>(C.centroids(i).Centroid(2)+jj+16) && (C.centroids(i).Centroid(2)+jj-15)>0 && volSize(3)>(C.centroids(i).Centroid(3)+kk+16) && (C.centroids(i).Centroid(3)+kk-15)>0)
                                    
                                    count = count +1;
                                    pic{count} = zcut(C.centroids(i).Centroid(2)+jj-15:C.centroids(i).Centroid(2)+jj+16, C.centroids(i).Centroid(1)+ii-15:C.centroids(i).Centroid(1)+ii+16);%xy image
                                    count = count +1;
                                    pic{count} = squeeze(xcut(C.centroids(i).Centroid(3)+kk-15:C.centroids(i).Centroid(3)+kk+16, C.centroids(i).Centroid(2)+jj-15:C.centroids(i).Centroid(2)+jj+16));%yz image
                                    count = count +1;
                                    pic{count} = squeeze(ycut(C.centroids(i).Centroid(3)+kk-15:C.centroids(i).Centroid(3)+kk+16, C.centroids(i).Centroid(1)+ii-15:C.centroids(i).Centroid(1)+ii+16));%xz image
                                    
                                    if (kk==1 && jj==1 && ii==1)
                                        cdata = cdata+1;
                                        pic = cat(3,pic{:});
                                        Train(cdata).data = single(pic);
                                        Train(cdata).label = 1;%nodule
                                        Train(cdata).origin = name;
                                        Train(cdata).nodule = C.centroids(i).Centroid;
                                    end
                                end
                            end
                        end
                        
                        
                    end
                end
                
                clear zcut xcut ycut zpic xpic ypic
                close all
            end
        end
        
        
    end
    
    
    
    %% select negstives
    negs = negativeCentroids(C(:).centroids, trueCentroid);
    disp('negs')
    length(negs)
    negs = vl_colsubset(negs,4000);
    disp('negs')
    length(negs)
    
    for i=1:length(negs),
        negs(i).Centroid(1) = floor(negs(i).Centroid(1));
        negs(i).Centroid(2) = floor(negs(i).Centroid(2));
        negs(i).Centroid(3) = floor(negs(i).Centroid(3));
        
        zcut = CT.vol(:,:,negs(i).Centroid(3));
        xcut = fliplr(rot90((squeeze(CT.vol(:,negs(i).Centroid(1),:))),3));
        ycut = fliplr(rot90((squeeze(CT.vol(negs(i).Centroid(2),:,:))),3));
        count = 0;
        pic = {};
        
                    kk=0;jj=0;ii=0;
                    if (volSize(1)>negs(i).Centroid(1)+16 && (negs(i).Centroid(1)-15)>0 && volSize(2)>(negs(i).Centroid(2)+16) && (negs(i).Centroid(2)-15)>0 && volSize(3)>(negs(i).Centroid(3)+16) && (negs(i).Centroid(3)-15)>0)
                        
                        count = count +1;
                        pic{count} = zcut(negs(i).Centroid(2)+jj-15:negs(i).Centroid(2)+jj+16, negs(i).Centroid(1)+ii-15:negs(i).Centroid(1)+ii+16);%xy image
                        count = count +1;
                        pic{count} = squeeze(xcut(negs(i).Centroid(3)+kk-15:negs(i).Centroid(3)+kk+16, negs(i).Centroid(2)+jj-15:negs(i).Centroid(2)+jj+16));%yz image
                        count = count +1;
                        pic{count} = squeeze(ycut(negs(i).Centroid(3)+kk-15:negs(i).Centroid(3)+kk+16, negs(i).Centroid(1)+ii-15:negs(i).Centroid(1)+ii+16));%xz image
                        
                       
                            cdatan = cdatan+1;
                            pic = cat(3,pic{:});
                            TrainN(cdatan).data = single(pic);
                            TrainN(cdatan).label = 0;%nodule
                            TrainN(cdatan).origin = name;
                            TrainN(cdatan).nodule = negs(i).Centroid;
                        
                        
                    end
    
        
    end
    
    clear C CT name disSize nodSize negs
    
    
end

save('TrainData_Pos_Neg.mat', 'Train','TrainN', '-v7.3')

