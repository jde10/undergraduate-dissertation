function feature_extraction_PHOW_HOG2 ()
clear all
close all
clc

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
run('/datos1/DATA/matlab/matconvnet/matlab/vl_setupnn')
% baseTrain = 'C:/Users/PC12/Documents/MATLAB/Im_Candidate_Nodules/Train/Validation/';
% baseTest = 'C:/Users/PC12/Documents/MATLAB/Im_Candidate_Nodules/Train/';
load ('AllTrainData_filter_Ann.mat');
numWords = 600;
numTest = 1174;% half of the training images selected for training are for validation
numTrain= 1035;% Half of the training images selected for training
model.numSpatialX = [1 3] ;
model.numSpatialY = [1 3] ;
model.quantizer = 'kdtree' ;

%--------------------------------------------------------------------------
% Part I : Organize Image Directory
%--------------------------------------------------------------------------
images = {};
imageClass = {};

% ims = dir(fullfile(baseTrain, '*.PNG'));
% ims = cellfun(@(x)fullfile(baseTrain,x),{ims.name},'UniformOutput', false);
% images = {images{:}, ims{:}};
% imageClass{end+1} = ones(1, length(ims));

selTrain = find(mod(0:length(Train)-1, numTrain+numTest) < numTrain) ;
selTest = setdiff(1:length(Train), selTrain);

%--------------------------------------------------------------------------
% Part II : Train Vocabulary
%--------------------------------------------------------------------------
% selTrainFeats = vl_colsubset(selTrain, 30);
% descrs = {};
% parfor ii = 1:length(selTrainFeats),
%     %im = imread(fullfile(images{selTrainFeats(ii)}));
%     S=load ('TrainData_filter_Ann.mat');
%     [~, im1] = vl_phow(im2single(S.Train(ii).data(:,:,1)), 'Sizes', 7, 'Step', 3);
%     [~, im2] = vl_phow(im2single(S.Train(ii).data(:,:,2)), 'Sizes', 7, 'Step', 3);
%     [~, im3] = vl_phow(im2single(S.Train(ii).data(:,:,3)), 'Sizes', 7, 'Step', 3);
%     descrs{ii} = [im1, im2, im3];
% end
% 
% save('descriptor.mat', 'descrs')
% descrs = cat(3, descrs);
% descrs = single(cell2mat(descrs));
% 
% %Get the visual words
% vocab = vl_kmeans(descrs, numWords,'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50);
% save('nod_PHOW_vocab.mat', 'vocab');
tic
load('nod_PHOW_vocab.mat')
model.vocab = vocab;
model.kdtree = vl_kdtreebuild(vocab) ;
W = load('trainHog.mat')
disp('Loading descriptors')
toc
% 
% %--------------------------------------------------------------------------
% % Part III : Compute Histograms
% %--------------------------------------------------------------------------
% tic  
% trainData = '/datos1/DATA/matlab/DATAV2/classification_data/Candidates_data_train.mat';
%   hists1 = {} ;
%   load(trainData)
% disp('Loading train data')
%   toc
%   feature = {};
   hogCellSize = 8;
%   hogScores={};
%   tic
%   parfor ii = 1:length(Train)
%     im1 = Train(ii).data(:,:,1);
%     im2 = Train(ii).data(:,:,2);
%     im3 = Train(ii).data(:,:,3);
%     h1 =getImageDescriptor(model, im1)
%     h2 =getImageDescriptor(model, im2)
%     h3 =getImageDescriptor(model, im3)
%     z = vl_hog(im2single(Train(ii).data(:,:,1)), hogCellSize);
%     x = vl_hog(im2single(Train(ii).data(:,:,2)), hogCellSize);
%     y = vl_hog(im2single(Train(ii).data(:,:,3)), hogCellSize);
%     imHog = [z,x,y];
%     hogScores= vl_nnconv(imHog,W.w,[]);
%     hists1{ii} = [h1; h2; h3; hogScores];
%     feature{ii} = Train(ii).label;
%   
%   end
%  disp('Training Train set')
%  toc
%   hists1 = cat(2, hists1{:}) ;
% 
%   histsN1 = {} ;
%     
%  %load(trainData);
%   featureN = {};
%   TrainN = vl_colsubset(TrainN, 25000);
%   tic
%   parfor ii = 1:length(TrainN)
%     im1 = TrainN(ii).data(:,:,1);
%     im2 = TrainN(ii).data(:,:,2);
%     im3 = TrainN(ii).data(:,:,3);
%     h1 =getImageDescriptor(model, im1)
%     h2 =getImageDescriptor(model, im2)
%     h3 =getImageDescriptor(model, im3)
%     z = vl_hog(im2single(TrainN(ii).data(:,:,1)), hogCellSize);
%     x = vl_hog(im2single(TrainN(ii).data(:,:,2)), hogCellSize);
%     y = vl_hog(im2single(TrainN(ii).data(:,:,3)), hogCellSize);
%     imHog = [z,x,y];
%     hogScoresN = vl_nnconv(imHog,W.w, []);
%     histsN1{ii} = [h1; h2; h3; hogScoresN];
%     featureN{ii} = TrainN(ii).label;
%  
% end
%   disp('Training TrainN set')
%   toc
%   histsN1 = cat(2, histsN1{:}) ;
%  
%  save('noduleHistogramsTrain.mat', 'hists1', 'histsN1', 'feature', 'featureN') ;
load('noduleHistogramsTrain.mat')

%--------------------------------------------------------------------------
% Part 4 : Compute feature map
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Part V : train SVM
%--------------------------------------------------------------------------
% numHardNegsIter = 5;
% sH = size(hists1);
% sHN = size(histsN1);
% xp = hists1;
% hardN = {};
% clear hists1;
% count = 0;
% countstd = 0;
% %Hard Negative Mining
% tic
% for t=1:numHardNegsIter,
%     numPos = sH(2);
%     numNeg = 5000;
%     sumt = ((t-1)*5000 +1);
%     sumtf = sumt+4999;
%     xn = histsN1(:,sumt:sumtf);
% 
%     histcat = [xp, xn];
% 
%     psix = vl_homkermap(histcat, 1, 'kchi2', 'gamma', .5) ;
%     
%     lambda = 1 / (10*(numPos+numNeg)) ;
%     w = [] ;
%     perm = randperm(numPos+numNeg) ;
%     y = [ones(numPos,1); (-1)*ones(numNeg,1)]';
%     [w b info] = vl_svmtrain(psix, y, lambda, ...
%         'Solver', 'sdca', ...
%         'MaxNumIterations', 50/lambda, ...
%         'BiasMultiplier', 1, ...
%         'Epsilon', 1e-3);
%     
%     model.b = 1 * b ;
%     model.w = w ;
%     
%     scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;
%     size(scores);
%     meanscore = mean(scores(1:numPos));
%     stdscore = std(scores(1:numPos));
%     for i=(numPos+1):numPos+numNeg,
%         if(scores(i)>=meanscore),
%             count = count + 1;
%             hardN{count} = histcat(:, i);
%         end
% 
%     end
% end
% disp('Hard Negatives SVM')
% toc
%   save('hardNegatives_std.mat', 'hardN');%, 'stdhardN') ;
%   hardN = cat(2, hardN{:});
% %Train Final SVM
% % N = load('hardNegatives_PHOW.mat');
% %stdN = load('hardNegatives_std.mat');
% 
% sN = size(hardN);
% 
% % substdN = vl_colsubset(hardN, 3105-sN(2));
% tic
% xn = hardN;
% clear hardN
% histcat = [xp, xn];
% 
% numPos = sH(2);
% numNeg = sN(2);
% 
% psix = vl_homkermap(histcat, 1, 'kchi2', 'gamma', .5) ;
% 
% lambda = 1 / (10*(numPos+numNeg)) ;
% w = [] ;
% perm = randperm(numPos+numNeg) ;
% y = [ones(numPos,1); (-1)*ones(numNeg,1)]';
% [w b info] = vl_svmtrain(psix(:, perm), y(perm), lambda, ...
%     'Solver', 'sdca', ...
%     'MaxNumIterations', 50/lambda, ...
%     'BiasMultiplier', 1, ...
%     'Epsilon', 1e-3);
% 
% model.b = 1 * b ;
% model.w = w ;
% model.info = info;
% 
% save('feature_SVM_model.mat', 'model')
% disp('Final SVM')
% toc
%--------------------------------------------------------------------------
% Part V : test SVM on validation
%--------------------------------------------------------------------------
tic  
validationData = '/datos1/DATA/matlab/DATAV2/classification_data/Candidates_data_validation.mat';
  load(validationData)
  hists1 = {} ;
  disp('Load Validation Data')
  toc
  feature = {};
  tic
  parfor ii = 1:length(Validation)
    im1 = Validation(ii).data(:,:,1);
    im2 = Validation(ii).data(:,:,2);
    im3 = Validation(ii).data(:,:,3);
    h1 =getImageDescriptor(model, im1)
    h2 =getImageDescriptor(model, im2)
    h3 =getImageDescriptor(model, im3)
    z = vl_hog(im2single(Validation(ii).data(:,:,1)), hogCellSize);
    x = vl_hog(im2single(Validation(ii).data(:,:,2)), hogCellSize);
    y = vl_hog(im2single(Validation(ii).data(:,:,3)), hogCellSize);
    imHog = [z,x,y];
    hogScores = vl_nnconv(imHog,W.w, []);
    hists1{ii} = [h1; h2; h3; hogScores];
    feature{ii} = Validation(ii).label;
    
  end
disp('Train Validation set')
 toc
  hists1 = cat(2, hists1{:}) ;

  histsN1 = {} ;
    
  ValidationN = vl_colsubset(ValidationN, 30000);
  featureN = {};
  tic
  parfor ii = 1:length(ValidationN)
    im1 = ValidationN(ii).data(:,:,1);
    im2 = ValidationN(ii).data(:,:,2);
    im3 = ValidationN(ii).data(:,:,3);
    h1 =getImageDescriptor(model, im1)
    h2 =getImageDescriptor(model, im2)
    h3 =getImageDescriptor(model, im3)
    z = vl_hog(im2single(ValidationN(ii).data(:,:,1)), hogCellSize);
    x = vl_hog(im2single(ValidationN(ii).data(:,:,2)), hogCellSize);
    y = vl_hog(im2single(ValidationN(ii).data(:,:,3)), hogCellSize);
    imHog = [z,x,y];
    hogScoresN = vl_nnconv(imHog,W.w, []);
    histsN1{ii} = [h1; h2; h3; hogScoresN];
    featureN{ii} = ValidationN(ii).label;
  end
  disp('Train ValidationN set')
toc
  histsN1 = cat(2, histsN1{:}) ;
 
  save('noduleHistogramsValidation.mat', 'hists1', 'histsN1', 'feature', 'featureN') ;
%  
%load('noduleHistogramsValidation.mat')
load('feature_SVM_model.mat')
tic
histcat=[hists1, histsN1];

psix = vl_homkermap(histcat, 1, 'kchi2', 'gamma', .5) ;
sP = size(hists1) ; sN = size(histsN1);
GT = [ones(1,sP(2)), zeros(1,sN(2))];

scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;
disp('Test Validation Set')
toc
save('noduleValidationScores.mat', 'GT', 'scores');


  % -------------------------------------------------------------------------
function hist = getImageDescriptor(model, im)
% -------------------------------------------------------------------------
% Author: Andrea Vedaldi

% Copyright (C) 2011-2013 Andrea Vedaldi
% All rights reserved.

im = im2single(im) ;
width = size(im,2) ;
height = size(im,1) ;
numWords = size(model.vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(im, 'Sizes', 7, 'Step', 3) ;

% quantize local descriptors into visual words
switch model.quantizer
  case 'vq'
    [~, binsa] = min(vl_alldist(model.vocab, single(descrs)), [], 1) ;
  case 'kdtree'
    binsa = double(vl_kdtreequery(model.kdtree, model.vocab, ...
                                  single(descrs), ...
                                  'MaxComparisons', 50)) ;
end

for i = 1:length(model.numSpatialX)
  binsx = vl_binsearch(linspace(1,width,model.numSpatialX(i)+1), frames(1,:)) ;
  binsy = vl_binsearch(linspace(1,height,model.numSpatialY(i)+1), frames(2,:)) ;

  % combined quantization
  bins = sub2ind([model.numSpatialY(i), model.numSpatialX(i), numWords], ...
                 binsy,binsx,binsa) ;
  hist = zeros(model.numSpatialY(i) * model.numSpatialX(i) * numWords, 1) ;
  hist = vl_binsum(hist, ones(size(bins)), bins) ;
  hists{i} = single(hist / sum(hist)) ;
end
hist = cat(1,hists{:}) ;
hist = hist / sum(hist) ;

