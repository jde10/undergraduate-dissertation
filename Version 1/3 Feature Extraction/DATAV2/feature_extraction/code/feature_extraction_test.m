function feature_extraction_test ()
clear all
close all
clc
parpool(4)
for k=106:503,
    feature_extraction_t(k)
    clearvars('*', '-except', 'k')
end

function feature_extraction_t (k)


run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
run('/datos1/DATA/matlab/matconvnet/matlab/vl_setupnn')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/test/';
baseD1 = '/datos1/DATA/matlab/DATAV2/preprocess_mask/test/'; %'C:/Users/PC12/Documents/LIDC-IDRI_STD01/Train/';
D0 = dir('/datos1/DATA/matlab/DATAV2/candidate_extraction/test/*.mat');
savingDirectory = '/datos1/DATA/matlab/DATAV2/feature_extraction/test_mat/';
scoreDirectory = '/datos1/DATA/matlab/DATAV2/feature_extraction/test_scores/';
dirSize = length(D0);
cdata=0;
cdatan=0;

tic
model.numSpatialX = [1 3];
model.numSpatialY = [1 3];
model.quantizer = 'kdtree';
W = load('trainHog.mat')
load('nod_PHOW_vocab.mat')
model.vocab = vocab;
model.kdtree = vl_kdtreebuild(vocab);
hogCellSize = 8;
disp('Load descriptors')
toc

% negs(1) = [1 1 1];

    
    %--------------------------------------------------------------------------
    % Part I : Load files
    %--------------------------------------------------------------------------
    tic
    C = load(strcat(baseD0,D0(k).name));
    CT = load(strcat(baseD1, strtok(D0(k).name, '.'), '.mat'));
    CT.vol = CT.lungData;
    disp('Load files')
    toc
    
    name = strtok(strtok(D0(k).name, '_'), '.');
    disp(name)
    %--------------------------------------------------------------------------
    % Part II : Extract XYZ Images of Nodules
    %--------------------------------------------------------------------------
    tic
    r=10;
    disSize = size(C.d);
    nodSize = size(C.nodule);
    volSize = size(CT.vol)
    for j=1:disSize(2)
        unique = 0;
        
        for i=1:numel(C.centroids),
            feature(i).Label = 0;
            feature(i).Centroid = C.centroids(i);
            if (C.minDist(j) <= r && C.d(i,j) == C.minDist(j))
                if(unique == 0)
                    trueCentroid(j)=i;
                    feature(i).Label = 1; % 1 = True Nodule
                    unique =1;
                    C.centroids(i).Centroid(1) = round(C.centroids(i).Centroid(1));
                    C.centroids(i).Centroid(2) = round(C.centroids(i).Centroid(2));
                    C.centroids(i).Centroid(3) = round(C.centroids(i).Centroid(3));
                    
                    zcut = CT.vol(:,:,C.centroids(i).Centroid(3));
                    xcut = fliplr(rot90((squeeze(CT.vol(:,C.centroids(i).Centroid(1),:))),3));
                    ycut = fliplr(rot90((squeeze(CT.vol(C.centroids(i).Centroid(2),:,:))),3));
                    
                    if (volSize(1)>C.centroids(i).Centroid(1)+16 && (C.centroids(i).Centroid(1)-15)>0 && volSize(2)>(C.centroids(i).Centroid(2)+16) && (C.centroids(i).Centroid(2)-15)>0 && volSize(3)>(C.centroids(i).Centroid(3)+16) && (C.centroids(i).Centroid(3)-15)>0)
                        cdata=cdata+1;
                        
                        pic(:,:,1) = zcut(C.centroids(i).Centroid(2)-15:C.centroids(i).Centroid(2)+16, C.centroids(i).Centroid(1)-15:C.centroids(i).Centroid(1)+16);%xy image
                        pic(:,:,2) = squeeze(xcut(C.centroids(i).Centroid(3)-15:C.centroids(i).Centroid(3)+16, C.centroids(i).Centroid(2)-15:C.centroids(i).Centroid(2)+16));%yz image
                        pic(:,:,3) = squeeze(ycut(C.centroids(i).Centroid(3)-15:C.centroids(i).Centroid(3)+16, C.centroids(i).Centroid(1)-15:C.centroids(i).Centroid(1)+16));%xz image
                        
                        Test(cdata).data = single(pic);
                        Test(cdata).label = 1;%nodule
                        Test(cdata).origin = name;
                        Test(cdata).nodule = C.centroids(i).Centroid;
                        
                    end
                end
                
                clear zcut xcut ycut zpic xpic ypic
                close all
            end
        end
        
        
    end
    disp ('Extract Candidates Test Positives')
    toc
    %% select negstives
    tic
    negs = negativeCentroids(C(:).centroids, trueCentroid);
    
    for i=1:length(negs),
        negs(i).Centroid(1) = floor(negs(i).Centroid(1));
        negs(i).Centroid(2) = floor(negs(i).Centroid(2));
        negs(i).Centroid(3) = floor(negs(i).Centroid(3));
        
        zcut = CT.vol(:,:,negs(i).Centroid(3));
        xcut = fliplr(rot90((squeeze(CT.vol(:,negs(i).Centroid(1),:))),3));
        ycut = fliplr(rot90((squeeze(CT.vol(negs(i).Centroid(2),:,:))),3));
        
        if (volSize(1)>negs(i).Centroid(1)+16 && (negs(i).Centroid(1)-15)>0 && volSize(2)>(negs(i).Centroid(2)+16) && (negs(i).Centroid(2)-15)>0 && volSize(3)>(negs(i).Centroid(3)+16) && (negs(i).Centroid(3)-15)>0)
            cdatan=cdatan+1;
            
            pic(:,:,1) = zcut(negs(i).Centroid(1)-15:negs(i).Centroid(1)+16, negs(i).Centroid(2)-15:negs(i).Centroid(2)+16);%xy image
            pic(:,:,2) = squeeze(xcut(negs(i).Centroid(3)-15:negs(i).Centroid(3)+16, negs(i).Centroid(2)-15:negs(i).Centroid(2)+16));%yz image
            pic(:,:,3) = squeeze(ycut(negs(i).Centroid(3)-15:negs(i).Centroid(3)+16, negs(i).Centroid(1)-15:negs(i).Centroid(1)+16));%xz image
            
            TestN(cdatan).data = single(pic);
            TestN(cdatan).label = 0;%nodule
            TestN(cdatan).origin = name;
            TestN(cdatan).nodule = negs(i);
            
            
        end
        
    end
    
    clear C CT disSize nodSize negs feature pic trueCentroid
    disp('Extract Candidates Test Negatives')
    toc
    %--------------------------------------------------------------------------
    % Part III : extract features
    %--------------------------------------------------------------------------
    
    hists1 = {} ;
    feature = {};
    centroid = {};
    tic
    parfor ii = 1:length(Test)
        im1 = Test(ii).data(:,:,1);
        im2 = Test(ii).data(:,:,2);
        im3 = Test(ii).data(:,:,3);
        h1 =getImageDescriptor(model, im1)
        h2 =getImageDescriptor(model, im2)
        h3 =getImageDescriptor(model, im3)
        z = vl_hog(im2single(Test(ii).data(:,:,1)), hogCellSize);
        x = vl_hog(im2single(Test(ii).data(:,:,2)), hogCellSize);
        y = vl_hog(im2single(Test(ii).data(:,:,3)), hogCellSize);
        imHog = [z,x,y];
        hogScores = vl_nnconv(imHog,W.w, []);
        hists1{ii} = [h1; h2; h3; hogScores];
        feature{ii} = Test(ii).label;
        centroid{ii} = Test(ii).nodule;
        
    end
    disp('Extract Features Test Positives')
    toc
    hists1 = cat(2, hists1{:}) ;
    histsN1 = {} ;
    centroidN = {};
    featureN = {};
    tic
    parfor ii = 1:length(TestN)
        im1 = TestN(ii).data(:,:,1);
        im2 = TestN(ii).data(:,:,2);
        im3 = TestN(ii).data(:,:,3);
        h1 =getImageDescriptor(model, im1)
        h2 =getImageDescriptor(model, im2)
        h3 =getImageDescriptor(model, im3)
        z = vl_hog(im2single(TestN(ii).data(:,:,1)), hogCellSize);
        x = vl_hog(im2single(TestN(ii).data(:,:,2)), hogCellSize);
        y = vl_hog(im2single(TestN(ii).data(:,:,3)), hogCellSize);
        imHog = [z,x,y];
        hogScoresN = vl_nnconv(imHog,W.w, []);
        histsN1{ii} = [h1; h2; h3; hogScoresN];
        featureN{ii} = TestN(ii).label;
        centroidN{ii} = TestN(ii).nodule
    end
    disp('Extract Features Test Negatives')
    toc

histsN1 = cat(2, histsN1{:}) ;

noduleHistogramsName = strcat(savingDirectory, name, '.mat')

save(noduleHistogramsName, 'hists1', 'histsN1', 'feature', 'featureN', 'centroid', 'centroidN', '-v7.3') ;
%--------------------------------------------------------------------------
    % Part IV : Test SVM
    %--------------------------------------------------------------------------
    

load('feature_SVM_model.mat')

histcat=[hists1, histsN1];

tic
psix = vl_homkermap(histcat, 1, 'kchi2', 'gamma', .5) ;
sP = size(hists1) ; sN = size(histsN1);
GT = [ones(1,sP(2)), zeros(1,sN(2))];

scores = model.w' * psix + model.b' * ones(1,size(psix,2));

nameScore = strcat(scoreDirectory, name, '.mat');
disp('Scores Extracted')
save(nameScore, 'GT', 'scores', '-v7.3');

toc




% -------------------------------------------------------------------------
function hist = getImageDescriptor(model, im)
% -------------------------------------------------------------------------
% Author: Andrea Vedaldi

% Copyright (C) 2011-2013 Andrea Vedaldi
% All rights reserved.

im = im2single(im) ;
width = size(im,2) ;
height = size(im,1) ;
numWords = size(model.vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(im, 'Sizes', 7, 'Step', 3) ;

% quantize local descriptors into visual words
switch model.quantizer
    case 'vq'
        [~, binsa] = min(vl_alldist(model.vocab, single(descrs)), [], 1) ;
    case 'kdtree'
        binsa = double(vl_kdtreequery(model.kdtree, model.vocab, ...
            single(descrs), ...
            'MaxComparisons', 50)) ;
end

for i = 1:length(model.numSpatialX)
    binsx = vl_binsearch(linspace(1,width,model.numSpatialX(i)+1), frames(1,:)) ;
    binsy = vl_binsearch(linspace(1,height,model.numSpatialY(i)+1), frames(2,:)) ;
    
    % combined quantization
    bins = sub2ind([model.numSpatialY(i), model.numSpatialX(i), numWords], ...
        binsy,binsx,binsa) ;
    hist = zeros(model.numSpatialY(i) * model.numSpatialX(i) * numWords, 1) ;
    hist = vl_binsum(hist, ones(size(bins)), bins) ;
    hists{i} = single(hist / sum(hist)) ;
end
hist = cat(1,hists{:}) ;
hist = hist / sum(hist) ;



