D= dir('/datos1/DATA/matlab/DATAV2/feature_extraction/test_scores/*.mat')
baseD = '/datos1/DATA/matlab/DATAV2/feature_extraction/test_scores/';


Thresh = -20:0.1:20;
TP = zeros(length(Thresh), 1); 
FN = zeros(length(Thresh), 1) ;
FP = zeros(length(Thresh), 1);
Precision = zeros(length(Thresh), 1);
Recall = zeros(length(Thresh), 1);
GTot= 0;


for k=1:length(D)
    load(strcat(baseD, D(k).name))
    disp(D(k).name)
    GTot= GTot + length(GT);
    for j=1:length(Thresh),
        S = scores>Thresh(j);
        for i=1:length(GT),
            if(GT(i) == double(S(i))),
                TP(j) = TP(j) +1;
            elseif (double(S(i) ==1) && GT(i) == 0)
                FP(j) = FP(j) +1;
            elseif (double(S(i) == 0) && GT(i) == 1)
                FN(j) = FN(j) + 1;
            end
        end
        clear S
    end
    clear scores GT
end
for j=1:length(Thresh)
Precision(j) = TP(j)/(TP(j)+FP(j));
Recall(j) = TP(j)/(TP(j)+FN(j));
end

figure(1)

plot(0.9742*Recall, Precision)
title('Precision Recall Test Set HOG and PHOW')
print('-dpng', 'PR_feature_test.png')