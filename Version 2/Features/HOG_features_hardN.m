function HOG_features_hardN()

setup ;

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')

load ('Train_annotations_jitter26.mat');

%--------------------------------------------------------------------------
% Part I : Obtain Nodule HOG Features
%--------------------------------------------------------------------------
hogCellSize = 8;
trainHog={};
count = 0
j = 0;
for i=1:length(Train)
       sData = size(Train(i).data);
     
       for k = 1:sData(4),
       count = count +1;
       im1 = vl_hog(im2single(Train(i).data(:,:,1,k)), hogCellSize);   
       im2 = vl_hog(im2single(Train(i).data(:,:,2,k)), hogCellSize);
       im3 = vl_hog(im2single(Train(i).data(:,:,3,k)), hogCellSize);

       trainHog{count} = [im1, im2, im3];

       clear im1 im2 im3
       end
   
end
size(trainHog{70})
trainHog = cat(4, trainHog{:});
%trainHog = cell2mat(trainHog);
w = mean(trainHog, 4);

save('trainHog.mat', 'w');
disp('done!!')
%load('/datos1/DATA/matlab/trainHog.mat')

% figure(1)
% imagesc(vl_hog('render',w));

%--------------------------------------------------------------------------
% Part II : Obtain Scores from detections
%--------------------------------------------------------------------------
load ('/datos1/DATA/matlab/nodules_bd_v05.mat');% 
load ('/datos2/DATA/matlab/DATAV4/feature_extraction/neg_hist.mat');

hogCellSize = 8;
hogScores={};
hogLabels= {};
hogFeatures = {};
j = 1;
count_neg = 0;

disp('calculating features')
for i=1:length(images.data)
    if(images.set(i)==1) %only train set
        z = vl_hog(im2single(images.data(:,:,1,i)), hogCellSize);
        x = vl_hog(im2single(images.data(:,:,2,i)), hogCellSize);
        y = vl_hog(im2single(images.data(:,:,3,i)), hogCellSize);
        imHog = [z,x,y];
        hogFeatures{j} = reshape(imHog, [], 1);
        if images.label(i) ==1
            hogLabels{j} = images.label(i);
        else
            if count_neg < 150074
                count_neg = count_neg +1;
                hogFeatures{j} = N_hist(:,count_neg);
            end
            hogLabels{j} = -1;
            hogScores{j} = vl_nnconv(imHog,w, []);
        end
        j=j+1;
    end
   clear z x y imHog
end
disp('done')

save('HogScores.mat', 'hogScores', 'hogFeatures', 'hogLabels', '-v7.3')
%load('HogScores.mat')
%--------------------------------------------------------------------------
% Part III : train SVM
%--------------------------------------------------------------------------
X = cat(2, hogFeatures{:});
if iscell(X)
    X=cell2mat(X);
end
Y = cat(1,hogLabels{:});
if iscell(Y)
    Y=cell2mat(Y);
end
lambda = 1 / (10*(length(X))) ;
psix = vl_homkermap(X, 3, 'kchi2', 'gamma', .5) ;

w = [];
[w b info] = vl_svmtrain(psix, Y, lambda, ...
    'Solver', 'sdca', ...
    'MaxNumIterations', 50/lambda, ...
    'BiasMultiplier', 1, ...
    'Epsilon', 1e-3);

model.b = 1 * b ;
model.w = w ;
model.info = info;
save('HOGmodelSVM.mat', 'model')
%load('HOGmodelSVM.mat')

%--------------------------------------------------------------------------
% Part IV : test SVM on validation
%--------------------------------------------------------------------------
%validationData = 'ValidationData_filter_Pos_Negs.mat';
%load(validationData)

%clear hogFeatures hogLabels hogScores X Y b w psix
hogCellSize = 8;
hogScores={};
j = 1;
for i=1:length(images.data)
    if(images.set(i)==2) %only val set
        z = vl_hog(im2single(images.data(:,:,1,i)), hogCellSize);
        x = vl_hog(im2single(images.data(:,:,2,i)), hogCellSize);
        y = vl_hog(im2single(images.data(:,:,3,i)), hogCellSize);
        imHog = [z,x,y];
        hogFeatures{j} = reshape(imHog, [], 1);
        if images.label(i) ==1
            hogLabels{j} = images.label(i);
        else
            hogLabels{j} = -1;
%            hogScores{j} = vl_nnconv(imHog,w, []);
        end
        j=j+1;
    end
   clear z x y imHog
end
disp('done')
		
%hogScores = cat(2,hogScores{:});
%hogScoresN = cat(2,hogScoresN{:});


save('HogScoresVal.mat', 'hogScores', 'hogFeatures', 'hogLabels', '-v7.3')

% load('noduleHistogramsValidation_HOG.mat')
%load('HOG_SVM_model.mat')
X = cat(2, hogFeatures{:});
if iscell(X)
    X=cell2mat(X);
end
GT = cat(1,hogLabels{:});
if iscell(GT)
    GT=cell2mat(GT);
end
% X = cat(2, hogFeatures{:});
% GT = cat(1,hogLabels{:});
lambda = 1 / (10*(length(X))) ;
psix = vl_homkermap(X, 3, 'kchi2', 'gamma', .5) ;
    
scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;

save('noduleValidationScoresHOG.mat', 'GT', 'scores', '-v7.3');

