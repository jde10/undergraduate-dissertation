function HOGfeature()

setup ;

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')

load ('Train_annotations_jitter26.mat');

%--------------------------------------------------------------------------
% Part I : Obtain Nodule HOG Features
%--------------------------------------------------------------------------
hogCellSize = 8;
trainHog={};
count = 0
j = 0;
for i=1:length(Train)
       sData = size(Train(i).data);
     
       for k = 1:sData(4),
       count = count +1;
       im1 = vl_hog(im2single(Train(i).data(:,:,1,k)), hogCellSize);   
       im2 = vl_hog(im2single(Train(i).data(:,:,2,k)), hogCellSize);
       im3 = vl_hog(im2single(Train(i).data(:,:,3,k)), hogCellSize);

       trainHog{count} = [im1, im2, im3];

       clear im1 im2 im3
       end
   
end
size(trainHog{70})
trainHog = cat(4, trainHog{:});
%trainHog = cell2mat(trainHog);
w = mean(trainHog, 4);

save('trainHog.mat', 'w');
disp('done!!')
load('trainHog.mat')

% figure(1)
% imagesc(vl_hog('render',w));

%--------------------------------------------------------------------------
% Part II : Obtain Scores from detections
%--------------------------------------------------------------------------
load ('nodules_bd_v05.mat');% 
hogCellSize = 8;
hogScores={};
hogLabels= {};
hogFeatures = {};
j = 0;

disp('calculating features')
for i=1:length(images.data)
       
       z = vl_hog(im2single(images.data(:,:,1,i)), hogCellSize);
       x = vl_hog(im2single(images.data(:,:,2,i)), hogCellSize);
       y = vl_hog(im2single(images.data(:,:,3,i)), hogCellSize);
       imHog = [z,x,y];
       hogFeatures{i} = reshape(imHog, [], 1);
       if images.label(i) ==1
       hogLabels{i} = images.label(i);
       else
           hogLabels{i} = -1;
       hogScores{i} = vl_nnconv(imHog,w, []);
       end
   clear z x y imHog
end
disp('done')
hogFeatures
hogScoresN={};
j = 0;
selTrainN = vl_colsubset(TrainN, 20000);
for i=1:length(selTrainN)
   if (length(unique(selTrainN(i).data(:)))> 1)
       j = j+1;
       z = vl_hog(im2single(selTrainN(i).data(:,:,1)), hogCellSize);
       x = vl_hog(im2single(selTrainN(i).data(:,:,2)), hogCellSize);
       y = vl_hog(im2single(selTrainN(i).data(:,:,3)), hogCellSize);
       imHog = [z,x,y];
       hogScoresN{j} = vl_nnconv(imHog,w, []);
   end
end

hogScores = cat(2,hogScores{:});
hogScoresN = cat(2,hogScoresN{:});
save('HogScores.mat', 'hogScores', 'hogFeatures', 'hogLabels', '-v7.3')
%load('HogScores.mat')
%--------------------------------------------------------------------------
% Part III : train SVM
%--------------------------------------------------------------------------
X = cat(2, hogFeatures{:});
Y = cat(1,hogLabels{:});
lambda = 1 / (10*(670277)) ;
psix = vl_homkermap(X(:, 1:670277), 1, 'kchi2', 'gamma', .5) ;

w = [];
[w b info] = vl_svmtrain(psix, Y(1:670277), lambda, ...
    'Solver', 'sdca', ...
    'MaxNumIterations', 50/lambda, ...
    'BiasMultiplier', 1, ...
    'Epsilon', 1e-3);

model.b = 1 * b ;
model.w = w ;
model.info = info;
save('HOGmodelSVM.mat', 'model')
load('HOGmodelSVM.mat')

%--------------------------------------------------------------------------
% Part IV : test SVM on validation
%--------------------------------------------------------------------------
validationData = 'ValidationData_filter_Pos_Negs.mat';
load(validationData)


hogCellSize = 8;
hogScores={};
j = 0;
for i=1:length(Validation)
   if (length(unique(Validation(i).data(:)))> 1)
       j = j+1;
       z = vl_hog(im2single(Validation(i).data(:,:,1)), hogCellSize);
       x = vl_hog(im2single(Validation(i).data(:,:,2)), hogCellSize);
       y = vl_hog(im2single(Validation(i).data(:,:,3)), hogCellSize);
       imHog = [z,x,y];
       hogScores{j} = vl_nnconv(imHog,w, []);
   end
end

hogScoresN={};
j = 0;

for i=1:length(ValidationN)
   if (length(unique(ValidationN(i).data(:)))> 1)
       j = j+1;
       z = vl_hog(im2single(ValidationN(i).data(:,:,1)), hogCellSize);
       x = vl_hog(im2single(ValidationN(i).data(:,:,2)), hogCellSize);
       y = vl_hog(im2single(ValidationN(i).data(:,:,3)), hogCellSize);
       imHog = [z,x,y];
       hogScoresN{j} = vl_nnconv(imHog,w, []);
   end
end
		
hogScores = cat(2,hogScores{:});
hogScoresN = cat(2,hogScoresN{:});


save('noduleHistogramsValidation_HOG.mat', 'hogScores', 'hogScoresN') ;

% load('noduleHistogramsValidation_HOG.mat')
%load('HOG_SVM_model.mat')

lambda = 1 / (10*(670277)) ;
psix = vl_homkermap(X(:, 670278:end), 1, 'kchi2', 'gamma', .5) ;

GT = Y(670278:end);
    
scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;

save('noduleValidationScoresHOG.mat', 'GT', 'scores', '-v7.3');

