
clear all
load('noduleValidationScoresPHOW.mat')

%GT = [ones(1,length(hogScores)), zeros(1,length(hogScoresN))];
Thresh = -20:0.1:20;
TP = zeros(length(Thresh), 1); 
TN = zeros(length(Thresh), 1); 

FN = zeros(length(Thresh), 1) ;
FP = zeros(length(Thresh), 1);

Precision = zeros(length(Thresh), 1);
Recall = zeros(length(Thresh), 1);

GTot = length(GT);
Class = zeros(length(Thresh),1);

for j=1:length(Thresh),
    S(j,:) = scores>Thresh(j);
    for i=1:GTot,
        if(GT(i)==1 && double(S(j,i))==1),
            TP(j) = TP(j) +1;
        elseif (double(S(j,i) ==1) && GT(i) == -1)
            FP(j) = FP(j) +1;
        elseif (double(S(j,i) == 0) && GT(i) == 1)
            FN(j) = FN(j) + 1;
        elseif (GT(i)==-1 && double(S(j,i))==0),
            TN(j) = TN(j) +1;    
        end
    end
    Precision(j) = TP(j)/(TP(j)+FP(j));
    Recall(j) = TP(j)/(TP(j) + FN(j));
    Class(j)=(FN(j)+FP(j))/(TP(j)+FN(j)+FP(j)+TN(j));
end

figure(1)
plot(0.9675*Recall, Precision, 'b')
hold on
plot(0.9675*Recall, Class, 'r')
title('Precision Recall Validation Set PHOW ')
print('-dpng', 'PR_feature_validation_PHOW.png')

save('scoresPHOWsubset.mat', 'Precision', 'Recall', 'Class', 'TP', 'FP', 'TN', 'FN', '-v7.3')


% figure(1)
% plot(0.9675*Recall, Class)
% title('Classification Error')
% print('-dpng', 'Classification_Error_PHOW.png')