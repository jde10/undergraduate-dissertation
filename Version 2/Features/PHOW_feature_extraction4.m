function PHOW_feature_extraction ()
clear all
close all
clc

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
disp('Loading variables')
load ('Train_annotations_jitter26.mat');
disp('done')
numWords = 600;
model.numSpatialX = [1 3] ;
model.numSpatialY = [1 3] ;
model.quantizer = 'kdtree' ;

%--------------------------------------------------------------------------
% Part I : Organize Image Directory
%--------------------------------------------------------------------------
images = {};
imageClass = {};

%--------------------------------------------------------------------------
% Part II : Train Vocabulary
%--------------------------------------------------------------------------
selTrainFeats = randi([1 1600], 1,40);
descrs = {};
im = {};
jj=0;
disp('Train Vocabulary')
for ii = 1:length(selTrainFeats),
    %S=load ('TrainData_filter_Ann.mat');
    sData = size(Train(selTrainFeats(ii)).data);
    for k=1:sData(4)
        jj=jj+1;
    [~, im1] = vl_phow(im2single(Train(selTrainFeats(ii)).data(:,:,1,k)), 'Sizes', 7, 'Step', 3);
    [~, im2] = vl_phow(im2single(Train(selTrainFeats(ii)).data(:,:,2,k)), 'Sizes', 7, 'Step', 3);
    [~, im3] = vl_phow(im2single(Train(selTrainFeats(ii)).data(:,:,3,k)), 'Sizes', 7, 'Step', 3);
    descrs{jj} = [im1, im2, im3];
    end
end

save('descriptor.mat', 'descrs')
descrs = cat(3, descrs{:});
descrs = single(descrs);
disp('done')
disp('Clustering')
%Get the visual words
vocab = vl_kmeans(descrs, numWords,'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50);
save('nod_PHOW_vocab.mat', 'vocab');
%load('nod_PHOW_vocab.mat')
model.vocab = vocab;
clear vocab descrs
model.kdtree = vl_kdtreebuild(model.vocab) ;
disp('done')
%--------------------------------------------------------------------------
% Part III : Compute Histograms
%--------------------------------------------------------------------------
  disp('loading variables')
  hists1 = {} ;
  load ('nodules_bd_v05.mat');
  feature = {};
  jj=0;
  disp('computing histograms')
  dp = 1:2:length(images.data);
  parpool(6)
  parfor ii = 1:length(dp)
        im1 = images.data(:,:,1,dp(ii));
        im2 = images.data(:,:,2,dp(ii));
        im3 = images.data(:,:,3,dp(ii));
        hists1{ii} = PHOW_Feature(im1, im2, im3, model);
        if images.label(dp(ii)) ==1
            feature{ii} = images.label(dp(ii));
        else
            feature{ii} = -1
        end
  end
  hists1 = cat(2, hists1{:}) ;
  feature = cat(1, feature{:});
 
  
  if  (iscell(hists1) ==1)
      hists1 = single(cell2mat(hists1));
  else
      hists1 = single(hists1);
  end
  
  clearvars('*', '-except', 'hists1', 'feature', 'model') 
save('noduleHistogramsTrain.mat', 'hists1', 'feature', '-v7.3') ;
% % load('noduleHistogramsTrain.mat')
 disp ('done')
%--------------------------------------------------------------------------
% Part IV : train SVM
%--------------------------------------------------------------------------
% 
count = 0;
countstd = 0;


    psix = vl_homkermap(hists1, 1, 'kchi2', 'gamma', .5) ;
    
    sHists1 = size(hists1);
    lambda = 1 / (10*(sHists1(2))) ;
    clear hists1
    w = [] ;
   disp('training SVM')
[w b info] = vl_svmtrain(psix, feature, lambda, ...
    'Solver', 'sdca', ...
    'MaxNumIterations', 50/lambda, ...
    'BiasMultiplier', 1, ...
    'Epsilon', 1e-3);

model.b = 1 * b ;
model.w = w ;
model.info = info;
save('PHOWmodelSVM.mat', 'model')
disp('done')
%load('PHOWmodelSVM.mat')
   clearvars('*', '-except', 'model')
%--------------------------------------------------------------------------
% Part V : test SVM on validation
%--------------------------------------------------------------------------
  hists1 = {} ;
  load ('nodules_bd_v05.mat');
  
  feature = {};
  disp('extract validation set histograms')
  dp = 2:2:length(images.data)
  
  parfor ii=1 : length(dp)
        im1 = images.data(:,:,1,dp(ii));
        im2 = images.data(:,:,2,dp(ii));
        im3 = images.data(:,:,3,dp(ii));
        hists1{ii} = PHOW_Feature(im1, im2, im3, model);
         if images.label(dp(ii)) ==1
            feature{ii} = images.label(dp(ii));
        else
            feature{ii} = -1
        end
  end
  hists1 = cat(2, hists1{:}) ;
  feature = cat(1, feature{:});
 
  
  if  (iscell(hists1) ==1)
      hists1 = single(cell2mat(hists1));
  else
      hists1 = single(hists1);
  end
  
    clearvars('*', '-except', 'hists1', 'feature', 'model') 
save('noduleHistogramsVal.mat', 'hists1', 'feature', '-v7.3') ;
% load('noduleHistogramsVal.mat')
disp('done')

sHists1 = size(hists1)
lambda = 1 / (10*(sHists1(2))) ;
psix = vl_homkermap(hists1, 1, 'kchi2', 'gamma', .5) ;
clear hists1

disp('calculating scores')    
scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;
GT = feature;
save('noduleValidationScoresPHOW.mat', 'GT', 'scores', '-v7.3');
disp('done')


  % -------------------------------------------------------------------------
function hist = getImageDescriptor(model, im)
% -------------------------------------------------------------------------
% Author: Andrea Vedaldi

% Copyright (C) 2011-2013 Andrea Vedaldi
% All rights reserved.

im = im2single(im) ;
width = size(im,2) ;
height = size(im,1) ;
numWords = size(model.vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(im, 'Sizes', 7, 'Step', 3) ;

% quantize local descriptors into visual words
switch model.quantizer
  case 'vq'
    [~, binsa] = min(vl_alldist(model.vocab, single(descrs)), [], 1) ;
  case 'kdtree'
    binsa = double(vl_kdtreequery(model.kdtree, model.vocab, ...
                                  single(descrs), ...
                                  'MaxComparisons', 50)) ;
end

for i = 1:length(model.numSpatialX)
  binsx = vl_binsearch(linspace(1,width,model.numSpatialX(i)+1), frames(1,:)) ;
  binsy = vl_binsearch(linspace(1,height,model.numSpatialY(i)+1), frames(2,:)) ;

  % combined quantization
  bins = sub2ind([model.numSpatialY(i), model.numSpatialX(i), numWords], ...
                 binsy,binsx,binsa) ;
  hist = zeros(model.numSpatialY(i) * model.numSpatialX(i) * numWords, 1) ;
  hist = vl_binsum(hist, ones(size(bins)), bins) ;
  hists{i} = single(hist / sum(hist)) ;
end
hist = cat(1,hists{:}) ;
hist = hist / sum(hist) ;
