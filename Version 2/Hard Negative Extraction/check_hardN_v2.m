function check = check_hardN_v2 (k, iter)
vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
D0 = dir(strcat(vol_dir,'*.mat'));
hardNDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_hardN/';
D1 = dir(strcat(hardNDirectory, '*.mat'));

B = struct2cell(D1);

name = strtok(strtok(D0(k).name, '_'), '.');
check = 0;
for i=1:iter,

direcSave = strcat(name,'_', num2str(i), '.mat');

if any(ismember(B(1,:), direcSave)) == 0
    if i<check || check == 0,
    check = i;
    end
end
end