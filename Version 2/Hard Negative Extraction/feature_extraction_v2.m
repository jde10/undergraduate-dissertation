function feature_extraction_v2 (k, jj)
%--------------------------------------------------------------------------
% Part III : extract features
%--------------------------------------------------------------------------

run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
run('/datos1/DATA/matlab/matconvnet/matlab/vl_setupnn')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';
baseD1 = '/datos1/DATA/matlab/DATAV2/preprocess_mask/train/'; %'C:/Users/PC12/Documents/LIDC-IDRI_STD01/Train/';
vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
D0 = dir(strcat(vol_dir,'*.mat'));
savingDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_mat/';
scoreDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_scores/';
pictureDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/picture_directory/train/';

dirSize = length(D0);
cdata=0;
cdatan=0;

tic
model.numSpatialX = [1 3];
model.numSpatialY = [1 3];
model.quantizer = 'kdtree';
W = load('/datos1/DATA/matlab/trainHog.mat');
load('/datos1/DATA/matlab/nod_PHOW_vocab.mat')
model.vocab = vocab;
model.kdtree = vl_kdtreebuild(vocab);
hogCellSize = 8;
disp('Load descriptors')
toc

name = strtok(strtok(D0(k).name, '_'), '.');

load(strcat(pictureDirectory, name,'_', num2str(jj), '.mat'))

hists1 = {} ;
label = {};
centroid = {};
tic
disp('Extracting features')
LL = length(images.label);
for ii=1:LL,
    z = vl_hog(im2single(images(1).data( : , : , 1 , ii)), hogCellSize);
    x = vl_hog(im2single(images(1).data( : , : , 2 , ii)), hogCellSize);
    y = vl_hog(im2single(images(1).data( : , : , 3 , ii)), hogCellSize);
    imHog = [z,x,y];
    imHog = reshape(imHog, [], 1);

    hists1{ii} = [imHog];
    
    if (images.label(ii) ==1),
        label{ii} = images.label(ii);
    else
        label{ii} = -1;
    end
        
end

hists1 = cat(2, hists1{:}) ;
label = cat(1, label{:});


if  (iscell(hists1) ==1)
    hists1 = single(cell2mat(hists1));
else
    hists1 = single(hists1);
end

disp('done')
noduleHistogramsName = strcat(savingDirectory, name,'_', num2str(jj), '.mat');

save(noduleHistogramsName, 'hists1', 'label', '-v7.3') ;
clear hists1 feature label

