function feature_svm_train_v2 (k, jj)
%--------------------------------------------------------------------------
% Part V : train SVM
%--------------------------------------------------------------------------
run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
run('/datos1/DATA/matlab/matconvnet/matlab/vl_setupnn')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';
baseD1 = '/datos1/DATA/matlab/DATAV2/preprocess_mask/train/'; %'C:/Users/PC12/Documents/LIDC-IDRI_STD01/Train/';
vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
D0 = dir(strcat(vol_dir,'*.mat'));
savingDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_mat/';
scoreDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_scores/';
hardNDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_hardN/';
dirSize = length(D0);
cdata=0;
cdatan=0;

tic
model.numSpatialX = [1 3];
model.numSpatialY = [1 3];
model.quantizer = 'kdtree';
W = load('/datos1/DATA/matlab/trainHog.mat');
load('/datos1/DATA/matlab/nod_PHOW_vocab.mat')
model.vocab = vocab;
model.kdtree = vl_kdtreebuild(vocab);
hogCellSize = 8;
disp('Load descriptors')
toc
name = strtok(strtok(D0(k).name, '_'), '.');
load(strcat(savingDirectory, name,'_', num2str(jj), '.mat'));

%Hard Negative Mining
tic

count = 0;
hardN= {};
sHists1 = size(hists1);

psix = vl_homkermap(hists1, 1, 'kchi2', 'gamma', .5) ;


lambda = 1 / (10*(sHists1(2))) ;
w = [] ;
disp('training SVM')
[w b info] = vl_svmtrain(psix, label, lambda, ...
    'Solver', 'sdca', ...
    'MaxNumIterations', 50/lambda, ...
    'BiasMultiplier', 1, ...
    'Epsilon', 1e-3);

model.b = 1 * b ;
model.w = w ;
model.info = info;

disp('done')

scores = model.w' * psix + model.b' * ones(1,size(psix,2)) ;
j=0;
for i=1:length(scores)
   if label(i)==1
       j=j+1;
       pos_scores(j) = scores(i);
   end
end

meanscore = mean(pos_scores(:));
stdscore = std(pos_scores(:));
for i=length(scores),
    if(scores(i)>=-0.2 && label(i)==-1),
        count = count + 1;
        hardN{count} = hists1(:, i);
    end 
end

disp('Hard Negatives SVM')
toc
direcSave = strcat(hardNDirectory, name,'_', num2str(jj), '.mat');
hardN = cat(2, hardN{:});
if exist('hardN', 'var')
save(direcSave, 'hardN', '-v7.3');
end
clear numPos numNeg scores hardN xp xn histcat psix y hists1 histsN1
