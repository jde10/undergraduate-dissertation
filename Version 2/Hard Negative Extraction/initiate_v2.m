function initiate_v2 (k)
vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
D = dir(strcat(vol_dir,'*.mat'));

iter = negative_candidates_v2(k);
check = check_hardN_v2(k,iter);
if check ~= 0,
    phrase = strcat(D(k).name, '_iteration_', num2str(check));
    disp(phrase)
    for jj = check:iter,
        subpicture_extraction_v2(k, jj)
        feature_extraction_v2(k, jj)
        feature_svm_train_v2 (k, jj)        
    end
end