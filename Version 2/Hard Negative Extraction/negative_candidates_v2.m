function iter = negative_candidates_v2 (k)
neg_dir = '/datos2/DATA/matlab/DATAV4/feature_extraction/neg_dir/';
sz = 16;
cand_dir = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';

vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
D = dir(strcat(vol_dir,'*.mat'));
nD = numel(D);
tmp_dir = 'tmp_images';
name = strtok(strtok(D(k).name, '_'), '.');

    out_name = fullfile(tmp_dir,D(k).name);

    images_vol = [];
    images_vol.data  = [];
    images_vol.set   = [];
    images_vol.label = [];
    
    
    tmp = load(fullfile(vol_dir, D(k).name));
    [tx, ty, tz] = size(tmp.vol);
    C = load(fullfile(cand_dir, D(k).name));
    md = min(C.d, [], 2);
    cands =cat(1, C.centroids.Centroid);
    cands = round(cands(:, [2 1 3]));
    if size(md,2)>0,
        good = (md > 20) & (cands(:,1) > sz) & ((tx - cands(:,1)) > sz) & (cands(:,2) > sz) & ((ty - cands(:,2)) > sz) & (cands(:,3) > sz) & ((tz - cands(:,3)) > sz) ;
    else
        good =  (cands(:,1) > sz) & ((tx - cands(:,1)) > sz) & (cands(:,2) > sz) & ((ty - cands(:,2)) > sz) & (cands(:,3) > sz) & ((tz - cands(:,3)) > sz) ;
    end
    
    neg_port = 50000; %% Amount of Negatives Per Pass
    iter = ceil(length(good)/neg_port); % Amount of iterations needed
    
    save_name = strcat(neg_dir, name, '.mat');
    save(save_name, 'iter', 'good', 'cands');