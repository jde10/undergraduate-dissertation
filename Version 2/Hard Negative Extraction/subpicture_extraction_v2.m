function subpicture_extraction_v2 (k, jj)
run('/datos1/DATA/matlab/vlfeat/toolbox/vl_setup')
run('/datos1/DATA/matlab/matconvnet/matlab/vl_setupnn')
baseD0 = '/datos1/DATA/matlab/DATAV2/candidate_extraction/train/';
D0 = dir('/datos1/DATA/matlab/DATAV2/candidate_extraction/train/*.mat');
savingDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_mat/';
scoreDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/train_scores/';
pictureDirectory = '/datos2/DATA/matlab/DATAV4/feature_extraction/picture_directory/train/';
dirSize = length(D0);
name = strtok(strtok(D0(k).name, '_'), '.');
cdata=0;
cdatan=0;
%--------------------------------------------------------------------------
% Part I : Load files
%--------------------------------------------------------------------------
%% load positives with jitter and reflect
images.data  = [];
images.set   = [];
images.label = [];

load('/datos1/DATA/matlab/Train_annotations_jitter26.mat')

I = cat(4, Train.data);
%I = cat(4, I, I(end:-1:1,:,:,:),I(:,end:-1:1,:,:),I(:,:,end:-1:1,:)); %Reflections
%I = cat(4, I, I(end:-1:1,:,:,:),I(:,end:-1:1,:,:),I(:,:,end:-1:1,:), I(end:-1:1,end:-1:1,:,:),I(end:-1:1,:,end:-1:1,:),I(:,end:-1:1,end:-1:1,:),I(end:-1:1,end:-1:1,end:-1:1,:));
num_train = size(I, 4);
tic
isneg = 0;
images.data  = cat(4, images.data, I);
images.set   = [images.set;   ones(size(I,4),1)];
images.label = [images.label; ones(size(I,4),1)];
clear Train I
%--------------------------------------------------------------------------
% Part II : Extract XYZ Images of Nodules
%--------------------------------------------------------------------------
%% sample and store training negatives
neg_dir = '/datos2/DATA/matlab/DATAV4/feature_extraction/neg_dir/';
vol_dir = '/datos1/DATA/LIDC-IDRI_STD02/train/';
neg_port = 50000;
D = dir(strcat(vol_dir,'*.mat'));
name = strtok(strtok(D(k).name, '_'), '.');
sz = 16;
load(strcat(neg_dir, name, '.mat'))

cands = cands(good, :);
    rp = randperm(size(cands,1));
    cands = cands(rp,:);
    
nD = numel(D);
tmp_dir = 'tmp_images';
name = strtok(strtok(D(k).name, '_'), '.');

    out_name = fullfile(tmp_dir,D(k).name);

    images_vol = [];
    images_vol.data  = [];
    images_vol.set   = [];
    images_vol.label = [];
    
    
    tmp = load(fullfile(vol_dir, D(k).name));
    [tx, ty, tz] = size(tmp.vol);
    
    I = [];
    
    if iter == 1
        startVal = 1;
        endVal = length(cands);
    else
        startVal = neg_port * (jj-1);
        if startVal == 0,
            startVal = 1;
        end
        endVal = neg_port * jj;
        if endVal>length(cands)
            endVal = length(cands);
        end
    end
    
    if startVal < endVal
        for kk = startVal:endVal
            isneg = 1;
            im1 = tmp.vol(cands(kk,1)+1-sz:sz+cands(kk,1), cands(kk,2)+1-sz:sz+cands(kk,2),cands(kk,3));
            im2 = squeeze(tmp.vol(cands(kk,1)+1-sz:sz+cands(kk,1),cands(kk,2), cands(kk,3)+1-sz:sz+cands(kk,3)));
            im3 = squeeze(tmp.vol(cands(kk,1), cands(kk,2)+1-sz:sz+cands(kk,2), cands(kk,3)+1-sz:sz+cands(kk,3)));
            im = cat(3,im1,im2,im3);
            I = cat(4,I,im);
        end
        images_vol.data  = cat(4,images_vol.data,I);
        images_vol.set   = [images_vol.set;   ones(size(I,4),1)];
        images_vol.label = [images_vol.label; 2*ones(size(I,4),1)];
    end
    disp(k)

disp('Extract Candidates Test Negatives')
toc

%% load all negatives, shuffle and save

images.data  = cat(4, images.data, images_vol.data);
images.set   = [images.set;  images_vol.set];
images.label = [images.label; images_vol.label];
disp(1i);



rp = randperm(numel(images.set));
images.set = images.set(rp);
images.label = images.label(rp);
images.data = images.data(:,:,:,rp);

direcName = strcat(pictureDirectory, name, '_', num2str(jj), '.mat');
save(direcName, 'images', 'isneg', 'iter', '-v7.3')
clear images images_vol
